<?php

class FTTManager_api {
	
	private $token;
	private $url;
	
	function __construct( $options, $token ) {
		
		$this->url   = $options['api_url'];
		$this->token = $token['body']->token;
		
	}
	
	public function get_all_tasks() {
		$url = $this->url . 'tasks';
		
		return $this->make_call( $url, 'get' );
	}
	
	public function getUsers() {
		$url = $this->url . 'users';
		
		return $this->make_call( $url, 'get' );
	}
	
	public function getTask( $taksId ) {
		$url = $this->url . 'task/' . $taksId;
		
		return $this->make_call( $url, 'get' );
	}
	
	public function delete( $taskId ) {
		$url = $this->url . 'task/delete/' . $taskId;
		
		return $this->make_call( $url, 'get' );
	}
	
	public function update() {
		$url      = $this->url . 'task/store';
		$postArgs = [
			'method' => 'POST',
			'body'   => $_POST
		];
		
		return $this->make_call( $url, 'get', $postArgs );
	}
	
	private function make_call( $url, $method, $args = null ) {
		
		$url .= '?token=' . $this->token;
		$callFunc     = "wp_remote_" . $method;
		$result       = $callFunc( $url, $args );
		$responseCode = wp_remote_retrieve_response_code( $result );
		if ( $responseCode == 200 ) {
			$response['body'] = json_decode( wp_remote_retrieve_body( $result ) );
		} else {
			$response['error'] = wp_remote_retrieve_response_message( $result );
		}
		
		return $response;
	}
}
	
	