<?php

class FTTManager {
	
	protected $loader;
	
	protected $plugin_name;
	
	protected $version;
	
	protected $options;
	
	protected $token;
	
	protected $api;
	
	
	public function __construct() {
		
		$this->plugin_name = 'fttmanager';
		$this->version     = '0.0.1';
		
		$this->options = get_option( $this->plugin_name . '_settings' );
		
		$this->getApiToken();
		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_rewrite_rules();
		$this->define_public_hooks();
		
	}
	
	private function load_dependencies() {
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-fttmanager-loader.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-fttmanager-admin.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-api.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-fttmanager-public.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-fttmanager-rewrite.php';
		
		$this->api    = new FTTManager_api( $this->options, $this->token );
		$this->loader = new FTTManager_Loader();
		
	}
	
	private function define_admin_hooks() {
		
		$plugin_admin = new FTTManager_Admin( $this->plugin_name, $this->version, $this->options, $this->token );
		
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_plugin_admin_menu' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'options_update' );
		
	}
	
	private function define_rewrite_rules() {
		
		$plugin_rewrite = new FTTManager_Rewrite( $this->plugin_name, $this->version, $this->options, $this->token );
		
		$this->loader->add_action( 'register_activation_hook', $plugin_rewrite, 'fttmanager_add_rewrite_rule' );
		$this->loader->add_action( 'query_vars', $plugin_rewrite, 'fttmanager_set_query_var' );
	}
	
	private function define_public_hooks() {
		
		$plugin_public = new FTTManager_Public( $this->plugin_name, $this->version, $this->token, $this->api, $this->options );
		
		$this->loader->add_action( 'init', $plugin_public, 'register_session' );
		$this->loader->add_action( 'init', $plugin_public, 'app_output_buffer' );
		
		if ( isset( $this->token['error'] ) ) {
			$this->loader->add_filter( 'the_content', $plugin_public, 'add_api_error_shortcode' );
			$this->loader->add_shortCode( 'api_error', $plugin_public, 'display_api_errors' );
		} else {
			$this->loader->add_action( 'template_include', $plugin_public, 'fttmanager_include_template' );
			$this->loader->add_filter( 'the_content', $plugin_public, 'render' );
			
		}
		
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_print_styles', $plugin_public, 'deregister_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		
		
	}
	
	public function run() {
		$this->loader->run();
	}
	
	public function get_plugin_name() {
		return $this->plugin_name;
	}
	
	public function get_loader() {
		return $this->loader;
	}
	
	public function get_version() {
		return $this->version;
	}
	
	public function getApiToken() {
		
		if ( isset( $this->options['api_url'] ) ) {
			$url          = $this->options['api_url'] . 'auth?email=' . $this->options['username'] . '&password=' . $this->options['password'];
			$result       = wp_remote_get( $url );
			$responseCode = wp_remote_retrieve_response_code( $result );
			if ( $responseCode == 200 ) {
				$response['body'] = json_decode( wp_remote_retrieve_body( $result ) );
			} else {
				$response['error'] = wp_remote_retrieve_response_message( $result );
			}
			
			$this->token = $response;
		}
	}
	
}
