<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://fttmanager.com
 * @since      1.0.0
 *
 * @package    FTTManager
 * @subpackage FTTManager/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    FTTManager
 * @subpackage FTTManager/includes
 * @author     FTTManager <hello@fttmanager.com>
 */
class FTTManager_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
