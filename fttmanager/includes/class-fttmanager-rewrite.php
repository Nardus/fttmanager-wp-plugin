<?php

class FTTManager_Rewrite {
	
	function fttmanager_add_rewrite_rule() {
		add_rewrite_rule( '^fttmanager?', 'index.php?is_fttmanager_page=1', 'top' );
	}
	
	function fttmanager_set_query_var( $vars ) {
		array_push( $vars, 'is_fttmanager_page' );
		array_push( $vars, 'ftt_action' );
		array_push( $vars, 'ftt_id' );
		
		return $vars;
	}
}