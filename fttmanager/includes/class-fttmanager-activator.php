<?php

/**
 * Fired during plugin activation
 *
 * @link       http://fttmanager.com
 * @since      1.0.0
 *
 * @package    FTTManager
 * @subpackage FTTManager/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    FTTManager
 * @subpackage FTTManager/includes
 * @author     FTTManager <hello@fttmanager.com>
 */
class FTTManager_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
