<?php
$html .= '<div class="table-wrapper">';
$html .= '<button class="btn btn-default" onclick="window.location=\'?is_fttmanager_page=1&ftt_action=get\'">Add Task</button>';
$html .= '<table class="table table-stiped" id="tasks-table" data-link="' . $this->options['api_url'] . '">';
$html .= '<thead>
					<tr>
						<th>ID</th>
						<th width="15%">User Name</th>
						<th>Task</th>
						<th>Status</th>
						<th>Description</th>
						<th width="15%">Due Date</th>
						<th></th>
					</tr>
				</thead>
				<tbody>';
foreach ( $results['body']->data as $result ) {
	$html .= '
				<tr>
					<td>' . $result->id . '</td>
					<td>' . $result->user_name . '</td>
					<td>' . $result->task . '</td>
					<td><div class="label label-'.$statusArray[$result->status][0].'">'.$statusArray[$result->status][1].'</div></td>
					<td>' . $result->task_description . '</td>
					<td>' . $result->due_date . '</td>
					<td>
					<div class="dropdown">
					    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    Options
						    <span class="caret"></span>
						</button>
					    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					        <li><a class="delete-task" data-id="' . $result->id . '" href="#">Delete</a></li>
					        <li><a class="edit-task"   data-id="' . $result->id . '" href="#">Edit</a></li>
					    </ul>
					</div>
					</td>
				</tr>
			';
}

$html .= '</tbody></div>';