<div class="container">
    <div class="col-md-12">
        <div class="col-md-4 col-md-offset-4">
            <form id="save-task-form" action="/?is_fttmanager_page=1&ftt_action=update" method="post">
                <div class="row">
                    <label for="user-name">User</label>
                    <select class="form-control" id="user_id" name="user_id">
                        <option>Select User...</option>
						<?php
						foreach ( $users['body']->data as $user ) {
							if ( isset( $task['body']->data->user_id ) && ( $user->id == $task['body']->data->user_id ) ) {
								echo "<option value='$user->id' selected>$user->name $user->surname</option>";
							} else {
								echo "<option value='$user->id'>$user->name $user->surname</option>";
							}
						}
						?>
                    </select>
                </div>
                <div class="row">
                    <label for="user-name">Task</label>
                    <input class="form-control" id="task" name="task" type="text"
                           value="<?= isset( $task['body']->data->task ) ? $task['body']->data->task : null ?>"/>
                </div>
                <div class="row">
                    <label for="user-name">Task</label>
                    <select class="form-control" id="status" name="status">
						<?php
						$statusArray = [
							1 => 'Pending',
							2 => 'In Progress',
							3 => 'Done'
						];
						foreach($statusArray as $key => $status) {
						    if(isset( $task['body']->data->task ) && ($key == $task['body']->data->task )) {
						        echo "<option value='$key' selected>$status</option>";
                            } else {
							    echo "<option value='$key'>$status</option>";
                            }
                        }
						 ?>

                    </select>
                </div>
                <div class="row">
                    <label for="user-name">Description</label>
                    <textarea class="form-control" cols="20" rows="4" id="task_description" name="task_description"
                              type="text"><?= isset( $task['body']->data->task_description ) ? $task['body']->data->task_description : null; ?></textarea>
                </div>
                <div class="row">
                    <label for="user-name">Due Date</label>
                    <input size="16" class="form-control form_datetime" name="due_date" type="text"
                           value="<?= isset( $task['body']->data->due_date ) ? $task['body']->data->due_date : null; ?>">
                    <span class="add-on"><i class="icon-th"></i></span>
                    <div class="row">
                        <div>&nbsp;</div>
                        <input type="hidden" name="id"
                               value="<?= isset( $task['body']->data->id ) ? $task['body']->data->id : null; ?>"/>
                        <button type="submit" class="btn btn-default">Save Task</button>
                        <button type="button" onclick="window.location='/?is_fttmanager_page=1'"
                                class="btn btn-default">
                            Cancel
                        </button>
                    </div>
            </form>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function ($) {
        $('.form_datetime').datetimepicker();
    });
</script>