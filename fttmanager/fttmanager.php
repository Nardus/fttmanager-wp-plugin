<?php
/**
 * Plugin Name: Task Manager FOGG Techinical
 *  * Plugin URI: http://www.example.com
 * Description: .
 * Version: 1.0.0
 * Author: Nardus Grobler
 * License: GPL2
 */

define( 'MY_PLUGIN_TEMPLATES', dirname( __FILE__ ) . '/templates' );

function activate_fttmanager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-fttmanager-activator.php';
	FTTManager_Activator::activate();
}

function deactivate_fttmanager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-fttmanager-deactivator.php';
	FTTManager_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'plugin_activation' );
register_deactivation_hook( __FILE__, 'plugin_deactivation');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-fttmanager.php';

function run_fttmanager() {
	
	$plugin = new FTTManager();
	$plugin->run();
	
}

class fttmanager_plugin extends WP_Widget {
	
	// constructor
	function fttmanager_plugin() {
		parent::__construct(false, $name = __('FTTManager', 'fttmanager') );
	}
	
	// widget form creation
	function form($instance) {
		echo"<input type='button' value='FTT Manager'>";
	}
	
	
	// widget display
	function widget($args, $instance) {
		echo"<input type='button' onclick=\"window.location='/?is_fttmanager_page=1'\" value='FTT Manager'>";
		
	}
	
}

add_action('widgets_init', create_function('', 'return register_widget("fttmanager_plugin");'));

run_fttmanager();