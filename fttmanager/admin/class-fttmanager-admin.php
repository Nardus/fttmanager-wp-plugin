<?php

class FTTManager_Admin {
	
	private $plugin_name;
	
	private $version;
	
	public function __construct( $plugin_name, $version, $options, $token ) {
		
		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->options     = $options;
		$this->token       = $token;
		
	}
	
	public function add_plugin_admin_menu() {
		add_options_page( 'FTTManager Settings', 'FTTManager', 'manage_options', $this->plugin_name,
			array( $this, 'display_plugin_setup_page' )
		);
	}
	
	public function add_action_links( $links ) {
		
		$settings_link = array(
			'<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' . __( 'Settings',
				$this->plugin_name ) . '</a>',
		);
		
		return array_merge( $settings_link, $links );
		
	}
	
	public function display_plugin_setup_page() {
		
		include_once( 'partials/fttmanager-admin-display.php' );
		
	}
	
	public function options_update() {
		
		register_setting( $this->plugin_name . '_settings_group', $this->plugin_name . '_settings' );
		
	}
}
