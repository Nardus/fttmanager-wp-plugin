<div id="fttmanager=" wrap">

<form method="post" action="options.php">
	
	<?php
	
	$options = $this->options;
	
	$api_url = isset( $options['api_url'] ) ? $options['api_url'] : '';
	?>
	
	<?php
	settings_fields( $this->plugin_name . '_settings_group' );
	?>

    <h4>FTTManager Settings</h4>
    <p>
        <label class="description" for="<?= $this->plugin_name; ?>_settings[api_url]">Enter Api Url</label>

        <input id="<?= $this->plugin_name; ?>_settings[api_url]" name="<?= $this->plugin_name; ?>_settings[api_url]"
               type="text" value="<?= $this->options['api_url'] ?>"/>
    </p>
    <p>
        <label class="description" for="<?= $this->plugin_name; ?>_settings[username]">Username: </label>

        <input id="<?= $this->plugin_name; ?>_settings[username]"
               name="<?= $this->plugin_name; ?>_settings[username]"
               type="text" value="<?= $this->options['username'] ?>"/>
    </p>
    <p>
        <label class="description" for="<?= $this->plugin_name; ?>_settings[password]">Password:</label>

        <input id="<?= $this->plugin_name; ?>_settings[password]"
               name="<?= $this->plugin_name; ?>_settings[password]"
               type="password" value="<?= $this->options['password'] ?>"/>
    </p>
    <p>
		<?php
		
		if ( isset( $this->token['error'] ) && !empty($this->token['error']) ) {
			echo "API Error: <strong>" . $this->token['error'] . "</strong>";
		} else if ( isset( $this->token['body'] ) ) {
			echo "<h4>Authentication Successful</h4>";
		}
		?>
    </p>
	
	<?php submit_button( __( 'Save Options', $this->plugin_name ), 'primary', 'Save', true ); ?>
</form>
</div>






