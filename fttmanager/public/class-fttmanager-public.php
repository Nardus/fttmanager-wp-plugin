<?php

class FTTManager_Public {
	
	
	private $plugin_name;
	
	private $version;
	
	private $token;
	
	private $api;
	
	private $options;
	
	public function __construct( $plugin_name, $version, $token, $api, $options ) {
		
		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->token       = $token;
		$this->api         = $api;
		$this->options     = $options;
		
	}
	
	function plugin_myown_template() {
		
		include( TEMPLATEPATH . "/page.php" );
		exit;
	}
	
	function app_output_buffer() {
		ob_start();
	}
	
	function register_session() {
		if ( ! session_id() ) {
			session_start();
		}
	}
	
	public function enqueue_styles() {
		
		if ( get_query_var( 'is_fttmanager_page' ) ) {
			
			wp_enqueue_style( $this->plugin_name . '-data-tables', plugin_dir_url( __FILE__ ) . 'css/data-tables.css',
				array(),
				$this->version, 'all' );
			
			wp_enqueue_style( $this->plugin_name . '-bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.css',
				array(),
				$this->version, 'all' );
			
			wp_enqueue_style( $this->plugin_name . '-datetime', plugin_dir_url( __FILE__ ) . 'css/bootstrap-datetimepicker.min.css',
				array(),
				$this->version, 'all' );
			
			wp_enqueue_style( $this->plugin_name . 'fttmanager-public',
				plugin_dir_url( __FILE__ ) . 'css/fttmanager-public.css',
				array(),
				$this->version, 'all' );
		}
		
	}
	
	public function deregister_styles() {
		if ( get_query_var( 'is_fttmanager_page' ) ) {
			wp_deregister_style( 'twentyseventeen-style' );
		}
	}
	
	public function enqueue_scripts() {
		
		if ( get_query_var( 'is_fttmanager_page' ) ) {
			wp_enqueue_script( $this->plugin_name . '-bootstrap', plugin_dir_url( __FILE__ ) . 'js/bootstrap.js',
				array( 'jquery' ), $this->version, false );
			wp_enqueue_script( $this->plugin_name . '-data-tables', plugin_dir_url( __FILE__ ) . 'js/data-tables.js',
				array( 'jquery' ), $this->version, false );
			wp_enqueue_script( $this->plugin_name . '-datetime', plugin_dir_url( __FILE__ ) . 'js/bootstrap-datetimepicker.min.js',
				array( 'jquery' ), $this->version, false );
			wp_enqueue_script( $this->plugin_name . '-fttmanager-public',
				plugin_dir_url( __FILE__ ) . 'js/fttmanager-public.js',
				array( 'jquery' ), $this->version, false );
		}
		
	}
	
	public function add_api_error_shortcode() {
		return '[api_error]';
	}
	
	public function display_api_errors() {
		return "<div class='error'>
    				<p>API Error" . $this->token['error'] . "</p>
				</div>";
	}
	
	function fttmanager_include_template( $template ) {
		if ( get_query_var( 'is_fttmanager_page' ) ) {
			$new_template = MY_PLUGIN_TEMPLATES . '/display-tasks.php';
			if ( file_exists( $new_template ) ) {
				$template = $new_template;
			}
		}
		
		return $template;
	}
	
	public function render( $content ) {
		$isFttManagerPage = get_query_var( 'is_fttmanager_page' ) ? get_query_var( 'is_fttmanager_page' ) : false;
		$action           = get_query_var( 'ftt_action' ) ? get_query_var( 'ftt_action' ) : false;
		$taskId           = get_query_var( 'ftt_id' ) ? get_query_var( 'ftt_id' ) : false;
		
		if ( $isFttManagerPage ) {
			if ( ! $action ) {
				$content = $this->genNotice( $content, $action );
				$content .= $this->display_tasks( $content );
				
				return $content;
			} else {
				$this->{$action . '_task'}( $taskId );
			}
		} else {
			return $content;
		}
	}
	
	public function genNotice( $content, $action ) {
		if ( isset( $_SESSION['api_error'] ) ) {
			$content .= "<div class='alert alert-danger'>" . ucfirst( $action ) . " failed!</div>";
			unset( $_SESSION['api_error'] );
		}
		if ( isset( $_SESSION['api_success'] ) ) {
			$content .= "<div class='alert alert-success'>" . ucfirst( $action ) . " success!</div>";
			unset( $_SESSION['api_success'] );
		}
		
		return $content;
	}
	
	public function update_task() {
		$results = $this->api->update( $_POST );
		
		if ( isset( $result['error'] ) ) {
			$_SESSION['api_error'] = $result['error'];
		} else {
			$_SESSION['api_success'] = 'Task successfully deleted';
		}
		
		wp_redirect( '/?is_fttmanager_page=1&cache='.rand() );
	}
	
	public function get_task( $taskId = null ) {
		if($taskId) {
			$task = $this->api->getTask( $taskId );
		}
		$users = $this->api->getUsers();
		
		include MY_PLUGIN_TEMPLATES . '/task-form.php';
	}
	
	public function delete_task( $taskId ) {
		$result = $this->api->delete( $taskId );
		if ( isset( $result['error'] ) ) {
			$_SESSION['api_error'] = $result['error'];
		} else {
			$_SESSION['api_success'] = 'Task successfully deleted';
		}
		
		wp_redirect( '/?is_fttmanager_page=1&cache='.rand() );
	}
	
	public function display_tasks( $content = null ) {
		$results = $this->api->get_all_tasks();
		$statusArray = [
			1 => ['warning','Pending'],
			2 => ['info','In Progress'],
			3 => ['success', 'Done']
		];
		$html = '<div class="table-borders">';
		include MY_PLUGIN_TEMPLATES . '/tasks-table.php';
		$html .= '</div>';
		return $html . $content;
	}
}
