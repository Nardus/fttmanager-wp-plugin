jQuery(document).ready(function ($) {
    $('#tasks-table').dataTable();

    $('.delete-task').on('click', function () {
        var taksId = $(this).data('id');

        window.location = '/?is_fttmanager_page=1&ftt_action=delete&ftt_id=' + taksId;
    });

    $('.edit-task').on('click', function () {
        var taksId = $(this).data('id');

        window.location = '/?is_fttmanager_page=1&ftt_action=get&ftt_id=' + taksId;
    });

    $('#datetimepicker1').datetimepicker();
});

function saveForm(elem) {
    var formData = jQuery('#save-task-form').serialize();

    window.location = '/?is_fttmanager_page=1&ftt_action=update' + formData;
};
